ARG NODE_VERSION
FROM node:$NODE_VERSION
RUN apk add --no-cache git && \
    git config --system user.name "Justin Voelker" && \
    git config --system user.email "justin@justinvoelker.com"

